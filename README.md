# Password Strength - zxcvbn.js

Password strength checker. User friendly interface with color codes.

Uses the zxcvbn.js library (includes password dictionaries, leaked passwords).

Standalone form example including Username, Password, Reveal password & Minimum Required Score input.

![Screenshot](https://gitlab.com/francoisjacquet/password-strength-zxcvbn-js/raw/master/screenshot.png)

[**Demo**](https://francoisjacquet.gitlab.io/password-strength-zxcvbn-js)

[zxcvbn.js official demo](https://lowe.github.io/tryzxcvbn/)

Dependencies:
- jquery.js
- zxcvbn.js

Version 1.0

Copyright 2019, François Jacquet

License GPLv2

[zxcvbn.js](https://github.com/dropbox/zxcvbn/) original license: MIT
